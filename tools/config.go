package tools

import (
	"encoding/json"
	"fmt"
	"os"
)

type Rtmp struct {
	BliBli   string //rtmp://live-push.bilivideo.com/live-bvc/
	KuaiShou string
	Dy       string
}
type Config struct {
	Rtmp      Rtmp   // 包含各大平台 rtmp
	StreamKey string // 推流密钥
	Fps       int    // 帧率
	FilePath  string // 文本 ，分割 播放列表
	WebPort   string // "8080"
	Lock      bool   // 目前还没用到
}

var Conf = new(Config)

//var Conf *Config

func InitConfig() *Config {
	//var Conf = new(Config)
	file, err := os.ReadFile("init.conf")
	if err != nil {
		fmt.Println("没有配置文件！")
		Conf.Rtmp.BliBli = "rtmp://live-push.bilivideo.com/live-bvc/"
		Conf.Fps = 30
		Conf.Lock = false
		Conf.WebPort = "8080"
		marshal, err := json.Marshal(&Conf)
		err = os.WriteFile("init.conf", marshal, 0666)
		if err != nil {
			fmt.Println("写入配置文件出现错误！")
			return nil
		}
		return Conf
	}
	err = json.Unmarshal(file, &Conf)
	if err != nil {
		fmt.Println("读取配置文件出现错误！")
		return nil
	}
	return Conf
}
