#!/bin/bash
# Program:
#  发布文件到Gitee
set -e

printf "\033[0;32m  updates to Gitee... \033[0m\n"

# hugo -D
# hugo -t nostyleplease -D

git add .
# Commit changes.
msg="于 $(date +'%Y-%m-%d %H:%M:%S') 上传"
if [ -n "$*" ]; then
    msg="$*"
fi
git commit -m "$msg"

git pull --rebase origin master
# Push source and build repos.
git push -f origin master
