package web

import (
	"blive/tools"
	"os"
	"path/filepath"
	"strings"
)

func player(path string) {
	var conf = tools.Conf
	key := conf.StreamKey
	dir, _ := os.ReadDir(path)
	rootpath := filepath.Dir(path)
	// 无限循环推流
	for true {
		// for 循环拼接 绝对路径
		for _, entry := range dir {
			fp := filepath.Join(rootpath, entry.Name())
			conf.FilePath += "," + fp
		}
		// 遍历取 其中一个视频地址
		list := strings.Split(conf.FilePath, ",")
		for _, s := range list {
			if s != "" {
				//fmt.Println("开始推流--->", s)
				tools.Command(s, key)
			}
		}
	}
}
